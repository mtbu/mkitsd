#!/bin/bash

## Copyright (C) 2023 Matthew Bunting
##
## Released subject to the terms of the MIT license, for full details see LICENSE
## file in this script's repository, https://gitlab.com/mtbu/mkitsd
##
## THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
## IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
## FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
## AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
## LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
## OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
## SOFTWARE.

# For more information on the iTunesSD file see http://www.ipodlinux.org/ITunesDB/#iTunesSD_file

PLAYLIST=playlist.m3u

VERSION="1.0"

ITUNESSD_FILE=iPod_Control/iTunes/iTunesSD

printf 'Welcome to %s\nVersion %s\n' "$0" "$VERSION"
printf '(C) 2023 Matthew Bunting\n\n' # Update as required

if [ $# -ge 1 ] ;then
  case "$1" in
    --help | -h | --usage)
      printf 'Usage: %s [options]\n' "$0"
      printf 'Arguments:\n'
      printf '  -p, --playlist <playlistfile>    Use a different playlist file\n'
      printf '  -h, --help, --usage              Display this help information\n'
      printf '\nThis tool generates the iTunesSD file for an iPod Shuffle (2nd generation)\nusing a playlist file listing music files (mp3, aac, wav) on the iPod.\n'
      printf '\nPlaylist files should contain raw file paths of music files relative to the iPod root directory.\nBlank lines and lines starting with "#" are ignored.\n'
      printf 'With no playlist specified this program will default to "%s"\n' "$PLAYLIST"
      exit 0
      ;;
    -p | --playlist)
      if [ "$2" = "" ] ;then
        printf 'Missing argument for --playlist\n'
        exit 1
      fi
      PLAYLIST="$2"
      ;;
    *)
      printf 'Unrecognised argument: %s\n' "$1"
      exit 1
      ;;
  esac
fi

cd $(dirname $0)
printf 'Working directory changed to "%s"\n' "$(pwd)"

if [ ! -f "$PLAYLIST" ] ;then
  printf 'Cannot find playlist file "%s"\n' "$PLAYLIST" >&2
  printf 'Make a playlist file or choose a different one with -p <playlistfile>\n' >&2
  exit 1
fi

TEST="${PLAYLIST:1:1}" || exit 1
# Shell happy indexing individual characters from string, continue
# Otherwise exit without clobbering the existing iTunesSD file


printf 'Using playlist file "%s"\n' "$PLAYLIST"

# Count up how many files are to be included
NUMFILES=0
while read LINE ;do
  case "$LINE" in
    \#* | '')
      ;;
    *)
      if [ -f "$LINE" ] ;then
        NUMFILES=$((NUMFILES + 1))
      fi
      ;;
  esac
done < "$PLAYLIST"

printf 'Processing %s files...\n\n' "$NUMFILES"

# Create the folder structure required
mkdir -p iPod_Control/iTunes

# Have to use printf twice or bash converts the numbers into characters, for more info see https://stackoverflow.com/questions/890262/integer-ascii-value-to-character-in-bash-using-printf
encodeValue3Byte() {
  VAL=$1
  SEQUENCE="$(printf '\\x%02x\\x%02x\\x%02x' $(((VAL >> 16) & 0xFF)) $(((VAL >> 8) & 0xFF)) $((VAL & 0xFF)) )"
  printf "$SEQUENCE"
}

# Write the header
#   song count  3bytes
#   unknown     3bytes (apparently use 0x010600 or 0x010800)
#   headersize  3bytes (18 = 0x12)
#   unknown     9bytes
writeHeader() {
  encodeValue3Byte $NUMFILES
  encodeValue3Byte 0x010800
  encodeValue3Byte 18
  encodeValue3Byte 0
  encodeValue3Byte 0
  encodeValue3Byte 0
}

# Write a song entry
#   entrysize 3bytes (0x22e)
#   unknown1  3bytes (0x5aa501)
#   starttime 3bytes (leave zero to play from start of track)
#   unknown2  3bytes (leave zero?)
#   unknown3  3bytes (leave zero?)
#   stoptime  3bytes (leave zero to play until end of track)
#   unknown4  3bytes (leave zero?)
#   unknown5  3bytes (leave zero?)
#   volume    3bytes (0x00 = -100%, 0x64 = 0%, 0xc8 = 100%)
#   filetype  3bytes (0x01 = MP3, 0x02 = AAC, 0x04 = WAV)
#   unknown6  3bytes (apparently 0x200)
#   filename     522 bytes (file path in UTF-16, pad end with 0x00, using forward slashes for directories like Linux does)
#   shuffleflag  1 byte (0x00 to avoid shuffling eg for podcasts, 0x01 to allow shuffling)
#   bookmarkflag 1 byte (0x00 to avoid bookmarking track, 0x01 to bookmark eg for podcasts)
#   unknownflag  1 byte (leave zero?)
writeSongEntry() {
  FILENAME="$1" # Argument is file name including path relative to ipod root directory eg /Music/Folder/track.mp3

  FILETYPE=0x00 # File type will be decided below based on file extension
  case "$FILENAME" in
    *.mp3 | *.MP3)
      FILETYPE=0x01
      ;;
    *.m4a | *.M4A | *.m4b | *.M4B | *.mp4 | *.MP4 | *.aac | *.AAC)
      FILETYPE=0x02
      ;;
    *.wav | *.WAV)
      FILETYPE=0x04
      ;;
  esac

  SHUFFLEFLAG=true   # Most tracks will want shuffling, logic further on to override
  BOOKMARKFLAG=false # Most tracks will not want bookmarking, logic further on to override
  case "$FILENAME" in /Podcasts/*) # Avoid shuffling but enable bookmarking of files within "Podcasts" folder
    SHUFFLEFLAG=false
    BOOKMARKFLAG=true
    ;;
  esac

  encodeValue3Byte 0x22e
  encodeValue3Byte 0x5aa501
  encodeValue3Byte 0
  encodeValue3Byte 0
  encodeValue3Byte 0
  encodeValue3Byte 0
  encodeValue3Byte 0
  encodeValue3Byte 0
  encodeValue3Byte 0x64
  encodeValue3Byte $FILETYPE
  encodeValue3Byte 0x200

  i=0
  while [ $i -lt 261 ] ;do
    CH="${FILENAME:$i:1}"
    if [ "$CH" = $'\x00' ] ;then
      break # Break from loop early if filename ends, then pad directly below. Speed improvement is miniscule but code has been written.
    else
      printf '%c\x00' "$CH"
      i=$((i+1))
    fi
  done
  while [ $i -lt 261 ] ;do # Pad remainder of filename field directly with zeroes
    printf '\x00\x00'
    i=$((i+1))
  done

  if [ $SHUFFLEFLAG = true ] ;then
    printf '\x01'
  else
    printf '\x00'
  fi

  if [ $BOOKMARKFLAG = true ] ;then
    printf '\x01'
  else
    printf '\x00'
  fi

 printf '\x00'
}

writeHeader > "$ITUNESSD_FILE"

while read LINE ;do
  case "$LINE" in
    # Ignore blank lines or lines beginning with #
    \#* | '')
      ;;
    *)
      # Handle lines beginning with ./ and / properly
      case "$LINE" in
        ./*)
          LINE="${LINE#./}" # Remove starting ./
          ;;
        /*)
          LINE="${LINE#/}" # Remove starting /
          ;;
      esac

      # Test if the file path exists, then add it
      if [ -f "$LINE" ] ;then
        printf 'Adding track "%s"\n' "$LINE" >&2
        writeSongEntry "/$LINE"
      fi
      ;;
  esac
done < "$PLAYLIST" >> "$ITUNESSD_FILE"
# Adding the stdout file redirect outside this loop makes a substantial speed improvement compared with redirecting the writeSongEntry function output directly, inside the loop

# If the iTunesPState file is present delete it so the ipod starts from the start of the playlist at default volume
if [ -f iPod_Control/iTunes/iTunesPState ] ;then
  rm iPod_Control/iTunes/iTunesPState
  printf 'Removing old iTunesPState file\n'
fi

# If the iTunesShuffle file (shuffled playlist sequence) is present delete it; the ipod regenerates this itself
if [ -f iPod_Control/iTunes/iTunesShuffle ] ;then
  rm iPod_Control/iTunes/iTunesShuffle
  printf 'Removing old iTunesShuffle file\n'
fi

