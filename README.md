# mkitsd

## What is it?
A utility for adding music to an iPod Shuffle 2nd generation using Linux.

While the iPod will mount as a USB storage device on a Linux system and
music can be copied directly to it, it will not recognise these files without
the correct database to describe them.

This tool generates that database (iTunesSD file) from a basic playlist file.
The playlist should contain all the tracks on the iPod, one filename per line,
with path relative to the iPod's root directory.

## Installation
1. Copy the ``mkitsd.sh`` script to the iPod's root directory.

## Usage
1. Mount the iPod and open a terminal at its mountpoint.
2. Copy music/podcasts/etc. onto the iPod. The directory structure is not important.
3. Create a list of these files, called ``playlist.m3u`` in the iPod root directory
    - This can be done manually, with a script,
or with a command like ``find -type f | sort > playlist.m3u`` and then edited by hand.
4. Run this tool with ``bash mkitsd.sh`` to generate the database.
5. Unmount/eject the iPod before disconnecting.

This tool resets the current playing state, so the iPod will return to
the first track with default volume level upon next start.

### Example playlist.m3u lines:
~~~
Music/Artist 1/Album 1/Track 1.mp3
/Music/Artist 1/Album 2/09. Another Track.wav

./Podcasts/Episode 245.mp3
Podcasts/another episode.m4a
# Music/Boring artist/A track I want ignored.mp3
~~~

## Dependencies
- bash, zsh, or similar shell

## Tips and Tricks
- A single file can be included multiple times at different places in the playlist.
- Files in the playlist that do not exist on the iPod will be ignored.
- Any files on the iPod which are not listed in the playlist file will be ignored.
- The tool will tell the iPod to "bookmark" any files within a ``Podcasts`` directory
in the iPod root directory, so it's possible to skip _forward_ to other tracks,
then return to the podcast at the same point it was left.
These will also be excluded when shuffling.
- The exact rules governing shuffle-exclude, bookmarking, etc. can be found
and configured within the script, in the ``writeSongEntry`` function.

## Limitations
- This tool doesn't correctly handle non-standard characters (like è) in filenames,
so any files including them should be renamed.
- Because the iPod uses an FAT filesystem the script will (probably) not be able
to be executed directly (``./mkitsd.sh``) and so should be executed like
``bash mkitsd.sh``

---

This has been tested on 2nd generation iPod Shuffle devices,
but might also work on 1st generation models.
It will not work on 3rd/4th generation models.

More detail about the iTunesSD file can be found at
[the iPodLinux wiki](http://www.ipodlinux.org/ITunesDB/#iTunesSD\_file).

This tool is inspired by [rebuild-db](https://shuffle-db.sourceforge.net/),
but written from scratch as a bash script for wider compatibility.
